(function () {
	'use strict';

	angular.module('BlurAdmin.pages', [
		'ui.router',

		'BlurAdmin.pages.dashboard',
		'BlurAdmin.pages.ui',
		'BlurAdmin.pages.components',
		'BlurAdmin.pages.form',
		'BlurAdmin.pages.tables',
		'BlurAdmin.pages.charts',
		'BlurAdmin.pages.maps',
		'BlurAdmin.pages.profile',
		
		'BlurAdmin.pages.hra.clients.active',
		'BlurAdmin.pages.hra.clients.archived',
		'BlurAdmin.pages.hra.clients.deleted',
		'BlurAdmin.pages.hra.roles',
		'BlurAdmin.pages.hra.adminProfile',
		'BlurAdmin.pages.hra.hraQ',
		'BlurAdmin.pages.hra',


	])
		.config(routeConfig);

	/** @ngInject */
	function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
		$urlRouterProvider.otherwise('/dashboard');

		baSidebarServiceProvider.addStaticItem({
			title: 'Edit Roles',
			// icon: 'ion-document',
			subMenu: [
			// 	{
			// 	title: 'Sign In',
			// 	fixedHref: 'auth.html',
			// 	blank: true
			// }, {
			// 	title: 'Sign Up',
			// 	fixedHref: 'reg.html',
			// 	blank: true
			// }, {
			// 	title: 'User Profile',
			// 	stateRef: 'profile'
			// },
				{
				title: 'Admin Profile',
				stateRef: 'adminProfile'
			}
			// , {
			// 	title: '404 Page',
			// 	fixedHref: '404.html',
			// 	blank: true
			// }
			]
		});
		// baSidebarServiceProvider.addStaticItem({
		// 	title: 'Menu Level 1',
		// 	icon: 'ion-ios-more',
		// 	subMenu: [{
		// 		title: 'Menu Level 1.1',
		// 		disabled: true
		// 	}, {
		// 		title: 'Menu Level 1.2',
		// 		subMenu: [{
		// 			title: 'Menu Level 1.2.1',
		// 			disabled: true
		// 		}]
		// 	}]
		// });
	}

}
)();
