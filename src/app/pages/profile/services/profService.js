(function() {
    'use strict';

    angular.module('BlurAdmin.pages.profile')
        .service('ProfileService', entrydataService);

    /** @ngInject */
    function entrydataService($q, $http,$rootScope) {

		var mapping = {get : Get};
		function Get(){
			return "dummy data";
		}
		return mapping;
	}
})();