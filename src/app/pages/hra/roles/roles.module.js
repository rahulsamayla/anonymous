(function () {
	'use strict';

	angular.module('BlurAdmin.pages.hra.roles', []).config(routeConfig);

	/** @ngInject */
	function routeConfig($stateProvider) {
		$stateProvider
			.state('roles', {
				url: '/hra/roles',
				templateUrl: 'app/pages/hra/roles/roles.html',
				title: 'Roles',
				sidebarMeta: {
					order: 800,
				},
			});
	}

}
)();
