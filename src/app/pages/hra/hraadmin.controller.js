/**
 * Created by timetraveller432 on 8/25/2017.
 */

(function () {

	angular
		.module('BlurAdmin.pages.hra')
		.controller('HraAdminController', HraAdminController);

	function HraAdminController($scope, SuperAdminService) {
		$scope.clients = [];

		SuperAdminService.getClients()
			.then(function (data) {
				$scope.clients = data;
			})
			.catch(function (err) {
				console.log(err);
			});
	}
}
)();