/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.hra.adminProfile', []).config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('adminProfile', {
          url: '/hra/adminProfile',
          title: 'Admin Profile',
          templateUrl: 'app/pages/hra/adminProfile/adminProfile.html',
          controller: 'adminProfilePageCtrl',
        });
  }

})();
