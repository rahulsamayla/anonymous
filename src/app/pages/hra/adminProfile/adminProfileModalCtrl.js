(function () {
  'use strict';

  angular.module('BlurAdmin.pages.hra.adminProfile')
    .controller('adminProfileModalCtrl', adminProfileModalCtrl);

  /** @ngInject */
  function adminProfileModalCtrl($scope, $uibModalInstance) {
    $scope.link = '';
    $scope.ok = function () {
      $uibModalInstance.close($scope.link);
    };
  }
})();