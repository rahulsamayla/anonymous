(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.hra',
     ['BlurAdmin.pages.hra.clients.active'])
        .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
          .state('admin', {
            url: '/hra/admin',
            title: 'Admin',
            templateUrl: 'app/pages/hra/hraadmin.html',
            controller: 'HraAdminController',
          });
    }
  
  })();
  