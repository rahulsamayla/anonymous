(function () {
	'use strict';
  
	angular.module('BlurAdmin.pages.hra.clients.active')
	  .controller('ActiveClientCTRL', ActiveClientCTRL);

	//ActiveClientCTRL.$inject = ['$scope' , '']
	/** @ngInject */
	function ActiveClientCTRL($scope , BackendService , toastr , $uibModal) {
	  
	
	  $scope.activeClients = [{
		  	'id' : 1,
			'company' : 'IBM India',
			'domain' : 'Information Technology', 
			'joined' : 'Mar 18',
			'campaigns' : 96,
			'users' : 1000
		},
		{
			'id' : 2,
			'company' : 'Infosys',
			'domain' : 'Business Process Outsourcing',
			'joined' : 'Jun 18',
			'campaigns' : 52,
			'users' : 5800
		},
		{
			'id' : 3,
			'company' : 'Royal Enfield',
			'domain' : 'Motorcycles',
			'joined' : 'Jan 18',
			'campaigns' : 23,
			'users' : 768
		},
		
		{
			'id' : 4,
			'company' : 'Ranbaxy',
			'domain' : 'Pharmaceuticals',
			'joined' : 'Feb 18',
			'campaigns' : 65,
			'users' : 6500
		}
		,
		{
			'id' : 5,
			'company' : 'NITI Ayog',
			'domain' : 'Indian Policy Planning',
			'joined' : 'Mar 18',
			'campaigns' : 12,
			'users' : 560
		},
		{
			'id': 6,
			'company' : 'Venera Ascension',
			'domain' : 'Space Exploration',
			'joined' : 'Aug 17',
			'campaigns' : 1,
			'users' : 46
		}
		,
		
		{
			'id' : 7,
			'company' : 'ACME',
			'domain' : ' High Performance Ballistics',
			'joined' : 'Dec 17',
			'campaigns' : 47,
			'users' : 2
		},
		
		{
			'id': 8,
			'company' : 'OCP',
			'domain' : 'Robotics',
			'joined' : 'Jun 17',
			'campaigns' : 34,
			'users' : 5000
		},
		
		];

		activate();
		
		function activate(){
			//BackendService
		}

		$scope.deleteClient = function(id){

			//original
			BackendService.deleteClient(id)
				.then(  ( response ) => {
					//show notification

				})
				.catch( ( err) => {
					console.error(err);
				})


			//mock	
			let index = $scope.activeClients.findIndex( ( client) => { return client.id == id ;} );
			if ( index !=-1)
				$scope.activeClients.splice( index , 1);
			toastr.success("Successfully Deleted!!" , 'Client');

		}

		$scope.addClient = function( client ){
			toastr.success("Successfully Added!!" , 'Client');
		}

		$scope.updateClient = function( client ){
			toastr.success("Successfully updated!!" , 'Client');
		}

		$scope.showUpdateForm = function(client){
			$scope.modalTitle = "Update Client";
			$scope.action = "update"
			$scope.selectedClient = client;
			open();
		}
		$scope.showAddClientForm = function( ){
			$scope.modalTitle = "Create new client";
			$scope.action = "add"
			open();
		}

		
		//opens the modal 
		// shared item are passed through 'resolve'

		function open() 	{
			
			let page = 'app/pages/hra/clients/modals/updateform.html';
			let size =  'md';

			$uibModal.open({
			  animation: true,
			  templateUrl: page,
			  controller : 'AddUpdateFormCTRL',
			  size: size,
			  resolve: {
				client: function () {
				  return $scope.selectedClient;
				},
				title: function () {
					return $scope.modalTitle;
				},
				action: function ( ) {
					return $scope.action;
				},
				add :  function(){
					return $scope.addClient;
				},
				update : function( ){
					return $scope.updateClient;
				}
			  }
			});
		  };




	}
})();