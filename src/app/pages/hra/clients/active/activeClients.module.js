(function () {
	'use strict';

	angular.module('BlurAdmin.pages.hra.clients.active', []).config(routeConfig);

	/** @ngInject */
	function routeConfig($stateProvider) {
		$stateProvider
			.state('activeClients', {
				url: '/hra/clients/active',
				templateUrl: 'app/pages/hra/clients/active/activeClients.html',
				controller : 'ActiveClientCTRL',	
				title: 'Active Clients',
				sidebarMeta: {
					order: 800,
				},
			});
	}

}
)();
