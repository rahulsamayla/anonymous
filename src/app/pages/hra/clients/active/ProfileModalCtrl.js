/**
 * @author a.demeshko
 * created on 21.01.2016
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.hra.clients.active')
    .controller('activeClientsModalCtrl', activeClientsModalCtrl);

  /** @ngInject */
  function activeClientsModalCtrl($scope, $uibModalInstance) {
    $scope.link = '';
    $scope.ok = function () {
      $uibModalInstance.close($scope.link);
    };
  }

})();