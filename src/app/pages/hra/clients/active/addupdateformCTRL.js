(function () {
	'use strict';
  
	angular.module('BlurAdmin.pages.hra.clients.active')
	  .controller('AddUpdateFormCTRL', ModalFormCTRL);

	//ActiveClientCTRL.$inject = ['$scope' , '']
	/** @ngInject */
	function ModalFormCTRL($scope , client, title , action , add , update )  {
		$scope.client = client;
		$scope.title = title;
		$scope.action = action;
		
		$scope.add = add;
		$scope.update = update
	}
})();
	  
	