(function () {
	'use strict';

	angular.module('BlurAdmin.pages.hra.clients.archived', []).config(routeConfig);

	/** @ngInject */
	function routeConfig($stateProvider) {
		$stateProvider
			.state('archivedClients', {
				url: '/hra/clients/archived',
				templateUrl: 'app/pages/hra/clients/archived/archivedClients.html',
				title: 'Archived Clients',
				controller : 'ArchivedClientCTRL',
				sidebarMeta: {
					order: 800,
				},
			});
	}
}
)();
