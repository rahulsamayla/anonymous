(function () {
	'use strict';
  
	angular.module('BlurAdmin.pages.hra.clients.archived')
	  .controller('ArchivedClientCTRL', ArchivedClientCTRL);
		
	//ActiveClientCTRL.$inject = ['$scope' , '']
	/** @ngInject */
	function ArchivedClientCTRL($scope , BackendService) {
	  
	
	  $scope.archivedClients = [{
			'company' : 'IBM India',
			'domain' : 'Information Technology', 
			'joined' : 'Mar 18',
			'campaigns' : 96,
			'users' : 1000
		},
		{
			'company' : 'Infosys',
			'domain' : 'Business Process Outsourcing',
			'joined' : 'Jun 18',
			'campaigns' : 52,
			'users' : 5800
		},
		
		{
			'company' : 'OCP',
			'domain' : 'Robotics',
			'joined' : 'Jun 17',
			'campaigns' : 34,
			'users' : 5000
		},
		
		];
	}
})();