(function () {
	'use strict';

	angular.module('BlurAdmin.pages.hra.clients.deleted', []).config(routeConfig);

	/** @ngInject */
	function routeConfig($stateProvider) {
		$stateProvider
			.state('deletedClients', {
				url: '/hra/clients/deleted',
				templateUrl: 'app/pages/hra/clients/deleted/deletedClients.html',
				title: 'Deleted Clients',
				controller : 'DeletedClientCTRL',
				sidebarMeta: {
					order: 800,
				},
			});
	}

}
)();
