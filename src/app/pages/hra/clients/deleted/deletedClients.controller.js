(function () {
	'use strict';
  
	angular.module('BlurAdmin.pages.hra.clients.deleted')
	  .controller('DeletedClientCTRL', DeletedClientCTRL);
		
	//ActiveClientCTRL.$inject = ['$scope' , '']
	/** @ngInject */
	function DeletedClientCTRL($scope , BackendService) {
	  
	
	  $scope.DeletedClients = [{
			'company' : 'RJ Corporation',
			'domain' : 'Beverages', 
			'joined' : 'Mar 18',
			'campaigns' : 96,
			'users' : 1000
		},
		{
			'company' : 'Swiggy',
			'domain' : 'Food Delivery',
			'joined' : 'Jun 18',
			'campaigns' : 52,
			'users' : 5800
		},
		
		{
			'company' : 'OCP',
			'domain' : 'Robotics',
			'joined' : 'Jun 17',
			'campaigns' : 34,
			'users' : 5000
		},
		
		];
	}
})();