/**
 * Created by timetraveller432 on 8/25/2017.
 */

(function () {
		var app = angular.module('BlurAdmin.pages.hra');
		app.service('HttpService', HttpService);

		function HttpService($http) {
			this.get = function (url, headers = {}) {
				return new Promise(function (resolve, reject) {
					$http({
						method: 'GET',
						url: url,
						headers: headers
					})
						.then(function (response) {
							resolve(response);
						})
						.catch(function (err) {
							reject(err);
						});
				});
			}

			this.post = function (url, data, headers = {}) {
				return new Promise(function (resolve, reject) {
					$http({
						method: 'POST',
						url: url,
						headers: headers,
						data: data
					})
						.then(function (response) {
							resolve(response);
						})
						.catch(function (err) {
							reject(err);
						});
				});
			}

			this.put = function (url, data, headers = {}) {
				return new Promise(function (resolve, reject) {
					$http({
						method: 'PUT',
						url: url,
						headers: headers,
						data: data
					})
						.then(function (response) {
							resolve(response);
						})
						.catch(function (err) {
							reject(err);
						});
				});
			}

			this.delete = function (url, headers = {}) {
				return new Promise(function (resolve, reject) {
					$http({
						method: 'DELETE',
						url: url,
						headers: headers
					})
						.then(function (response) {
							resolve(response);
						})
						.catch(function (err) {
							reject(err);
						});
				});
			}
		}
	}
)();