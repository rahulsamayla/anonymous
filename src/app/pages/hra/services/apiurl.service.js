/**
 * Created by timetraveller432 on 8/25/2017.
 */

(function () {
	var app = angular.module('BlurAdmin.pages.hra');
	app.service('ApiUrls', ApiUrls);

	function ApiUrls(BASE_API_URL) {
		this.adminupdate = function (adminId) {
			return BASE_API_URL + '/admins/' + adminId;
		}

		this.admindelete = function (adminId) {
			return BASE_API_URL + '/admins/' + adminId;
		}

		this.adminget = function (adminId) {
			return BASE_API_URL + '/admins/' + adminId;
		}

		this.admingetall = function () {
			return BASE_API_URL + '/admins';
		}

		this.OrganisationGetAll = function () {
			return BASE_API_URL + '/organisations';
		}

		this.OrganisationGet = function (organisationid) {
			return BASE_API_URL + '/organisations/' + organisationid;
		}

		this.OrganisationUpdate = function (organisationid) {
			return BASE_API_URL + '/organisations/' + organisationid;
		}

		this.OrganisationDelete = function (organisationid) {
			return BASE_API_URL + '/organisations/' + organisationid;
		}

		this.OrganisationCreate = function (organisationid) {
			return BASE_API_URL + '/organisations';
		}

		this.OrganisationSetAdmin = function (organisationid) {
			return BASE_API_URL + '/organisations';
		}

		this.OrganisationDeleteAdmin = function (organisationid) {
			return BASE_API_URL + '/organisations';
		}

	}
}
)();