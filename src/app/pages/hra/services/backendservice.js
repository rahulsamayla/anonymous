(function() {
    'use strict';

    angular.module('BlurAdmin.pages.hra')
        .service('BackendService', BackendService);

    /** @ngInject */
    function BackendService( HttpService , ApiUrls) {

       
		this.createClient = function (data) {
			return new Promise(function (resolve, reject) {
				HttpService.post(ApiUrls.OrganisationCreate(), data)
					.then(function (response) {
						resolve(response);
					})
					.catch(function (err) {
						reject(err);
					});
			});
		};
		//-------------------------------------------------------------------------------------
		// Listing
		this.getClients = function () {
			return new Promise(function (resolve, reject) {
				HttpService.get(ApiUrls.OrganisationGetAll())
					.then(function (response) {
						resolve(response);
					})
					.catch(function (err) {
						reject(err);
					});
			});
		};

		this.getClient = function (id) {
			return new Promise(function (resolve, reject) {
				HttpService.get(ApiUrls.OrganisationGet(id))
					.then(function (response) {
						resolve(response);
					})
					.catch(function (err) {
						reject(err);
					});
			});
		};
		//-------------------------------------------------------------------------------------
		// Updating
		this.updateClient = function (id, data) {
			return new Promise(function (resolve, reject) {
				HttpService.get(ApiUrls.OrganisationUpdate(id), data)
					.then(function (response) {
						resolve(response);
					})
					.catch(function (err) {
						reject(err);
					});
			});
		};
		//-------------------------------------------------------------------------------------
		// Deletion
		this.deleteClient = function (id) {
			return new Promise(function (resolve, reject) {
				HttpService.get(ApiUrls.OrganisationDelete(id))
					.then(function (response) {
						resolve(response);
					})
					.catch(function (err) {
						reject(err);
					});
			});
		}

		
	}
})();