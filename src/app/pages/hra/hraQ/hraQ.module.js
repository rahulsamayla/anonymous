(function () {
	'use strict';

	angular.module('BlurAdmin.pages.hra.hraQ', []).config(routeConfig);

	/** @ngInject */
	function routeConfig($stateProvider) {
		$stateProvider
			.state('hraQ', {
				url: '/hra/hraQ',
				templateUrl: 'app/pages/hra/hraQ/hraQ.html',
				title: 'HRA Questionnaire',
				sidebarMeta: {
					order: 800,
				},
			});
	}

}
)();
